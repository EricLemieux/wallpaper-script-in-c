//Wallpaper gererating program, rewritten in C++ because python took forever to generate anything.

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

template <typename T> T LERP(T v0, T v1, float dt)
{
	return ((1-dt)*v0 + dt*v1);	
}

struct pixel
{
	float r,g,b;
	pixel()
	{
		r = float(rand())/float(RAND_MAX);
		g = float(rand())/float(RAND_MAX);
		b = float(rand())/float(RAND_MAX);
	}
};

void WriteToFile(const pixel* image, const unsigned int WIDTH, const unsigned int HEIGHT)
{
	std::fstream f;
	f.open("/tmp/wallpaper.ppm", std::fstream::out);
	std::string buffer;
	char header[256];
	sprintf(header, "P3 %d %d %d \n", WIDTH, HEIGHT, 255);
	buffer += header;
	for(unsigned int i = 0; i < WIDTH * HEIGHT; ++i)
	{
		char str[256];
		sprintf(str, "%d %d %d \n", int(image[i].r*255), int(image[i].g*255), int(image[i].b*255));
		buffer += str;
	}
	f.write(buffer.c_str(), buffer.size());
}

bool AnyStringMatches(const char* str, const char** dictionary, int length)
{
	for(unsigned int i = 0; i < length; ++i)
	{
		if(!strcmp(str, dictionary[i]))
			return true;
	}
	return false;
}

int main(int argc, char** argv)
{
	srand(time(NULL));
	//Assume 1920x1080 unless specified in the arguments
	unsigned int WIDTH = 1920;
	unsigned int HEIGHT = 1080;

	int width_flag = -1, height_flag = -1;

	const char *widthKeywords[] = {"width", "w"};
	const char *heightKeywords[] = {"height", "h"};

	for(unsigned int i = 0; i < argc; ++i)
	{
		//Read through the arguments
		std::cout<<"arg num "<<i<<" "<<argv[i]<<std::endl;

		if(AnyStringMatches(argv[i], widthKeywords, 2))
			width_flag = i+1;
		else if(AnyStringMatches(argv[i], heightKeywords, 2))
			height_flag = i+1;

		if(width_flag == i)
			sscanf(argv[i], "%i", &WIDTH);
		if(height_flag == i)
			sscanf(argv[i], "%i", &HEIGHT);
	}
	std::cout<<"Width: "<<WIDTH<<"\nHeight: "<<HEIGHT<<std::endl;
	const unsigned int BUFFER_SIZE = WIDTH * HEIGHT;
	pixel *image;
	image = (pixel*) malloc(WIDTH * HEIGHT * sizeof(pixel));

	//Random some values that I will interpolate to
	pixel START;
	pixel TOP_RIGHT = pixel();
	pixel BOT_LEFT = pixel();

	std::cout<<"START: "<<START.r*255<<", "<<START.g*255<<", "<<START.b*255<<"\n";
	std::cout<<"TOP_RIGHT: "<<TOP_RIGHT.r*255<<", "<<TOP_RIGHT.g*255<<", "<<TOP_RIGHT.b*255<<"\n";
	std::cout<<"BOT_LEFT: "<<BOT_LEFT.r*255<<", "<<BOT_LEFT.g*255<<", "<<BOT_LEFT.b*255<<"\n";

	for(unsigned int h = 0; h < HEIGHT; h++)
	{
		for(unsigned int w = 0; w < WIDTH; w++)
		{
			//TODO make this less shit so its all one call
			image[h*WIDTH + w].r = (LERP(START.r, TOP_RIGHT.r, float(w)/float(WIDTH)) + LERP(START.r, BOT_LEFT.r, float(h)/float(HEIGHT)))/2;
			image[h*WIDTH + w].g = (LERP(START.r, TOP_RIGHT.g, float(w)/float(WIDTH)) + LERP(START.g, BOT_LEFT.g, float(h)/float(HEIGHT)))/2;
			image[h*WIDTH + w].b = (LERP(START.r, TOP_RIGHT.b, float(w)/float(WIDTH)) + LERP(START.b, BOT_LEFT.b, float(h)/float(HEIGHT)))/2;
		}
	}

	//Write the image to the file
	WriteToFile(image, WIDTH, HEIGHT);

	return 0;
}
